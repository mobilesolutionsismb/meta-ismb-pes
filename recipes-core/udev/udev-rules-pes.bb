DESCRIPTION = "udev rules for LibNfc hack"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690"

SRC_URI = " file://01-pes.rules"

S = "${WORKDIR}"

do_install () {
        install -d ${D}${sysconfdir}/udev/rules.d
        install -m 0644 ${WORKDIR}/01-pes.rules ${D}${sysconfdir}/udev/rules.d/
}
