alias ll='ls -la --color=auto'

alias io='io_all | grep sysfs'
alias io_all='cat /sys/kernel/debug/gpio'
alias pwm='cat /sys/kernel/debug/pwm'

alias NetRestart0='ifdown eth0 && ifup eth0'
alias NetRestart1='ifdown eth1 && ifup eth1'

alias install_ull='install_yocto.sh -b mx6ull -r emmc'
alias install_ul='install_yocto.sh -b mx6ul -r emmc'

alias displayOff='echo 1 > /sys/class/graphics/fb0/blank'
alias displayOn='echo 0 > /sys/class/graphics/fb0/blank'

export XDG_RUNTIME_DIR=/run/user/$(id -u)
export QT_QPA_PLATFORM=wayland

ifdown eth1 && ifup eth1