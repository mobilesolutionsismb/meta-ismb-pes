FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append = " \
	file://gpio_init \
	file://.profile \
	file://media-by-label-auto-mount.rules \
	file://main.conf \
	"
	
do_install_append() {
	if [ -e "${WORKDIR}/gpio_init" ]; then
		install -m 0755    ${WORKDIR}/gpio_init ${D}${sysconfdir}/init.d
		update-rc.d -r ${D} gpio_init start 1 2 3 4 5 .
		
	fi
	
	
	install -d ${D}/etc/udev/rules.d/
    install -m 0755 ${WORKDIR}/media-by-label-auto-mount.rules ${D}/etc/udev/rules.d/
    install -d ${D}/home/root/
    install -m 0755 ${WORKDIR}/.profile ${D}/home/root/
    install -d ${D}/etc/connman/
    install -m 0755 ${WORKDIR}/main.conf ${D}/etc/connman
    
}

FILES_${PN} += " \
	/home/root/.profile \
	/etc/connman/main.conf \
"