FILESEXTRAPATHS_prepend_imx6ul-var-dart := "${THISDIR}/files:"
SRC_URI += "file://pes_dts.dtsi"

PES_DTS_imx6ul-var-dart = "${WORKDIR}/pes_dts.dtsi"

do_preconfigure_prepend() {
   cp ${PES_DTS} ${S}/arch/${ARCH}/boot/dts/imx6ul-imx6ull-var-dart-common.dtsi
}