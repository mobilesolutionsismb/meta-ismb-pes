```console
.
|-- conf
|   |-- distro
|   |   `-- fslc-framebuffer-rcs.conf
|   |-- layer.conf
|   `-- machine
|       `-- imx6ul-var-dart.conf
|-- COPYING.MIT
|-- README
|-- recipes-bsp
|   `-- alsa-state
|       |-- alsa-state
|       |   `-- imx6ul-var-dart
|       `-- alsa-state.bbappend
|-- recipes-core
|   |-- init-ifupdown
|   |   |-- files
|   |   |   `-- interfaces
|   |   `-- init-ifupdown_1.0.bbappend
|   `-- initscripts
|       |-- files
|       |   |-- adau1761.bin
|       |   |-- gpio_init
|       |   |-- init_hw
|       |   |-- media-by-label-auto-mount.rules
|       |   `-- pulse_start
|       `-- initscripts_%.bbappend
|-- recipes-extended
|   |-- cronie
|   |   |-- cronie
|   |   |   `-- crontab
|   |   `-- cronie_%.bbappend
|   `-- rcs-launcher
|       |-- files
|       |   `-- rcsdevice.sh
|       `-- rcs-launcher.bb
|-- recipes-fsl
|   `-- images
|       |-- fsl-core-rcs.bb
|       `-- fsl-image-ismb.bb
|-- recipes-kernel
|   `-- linux
|       |-- files
|       |   |-- adau_imu_defconfig
|       |   `-- adau_imu.diff
|       `-- linux-variscite_4.1.15.bbappend
|-- recipes-multimedia
|   |-- libopus
|   |   |-- libopus_1.1.3.bb
|   |   `-- libopus_1.2.1.bb
|   `-- libopusenc
|       `-- libopusenc_0.1.1.bb
|-- scripts
|   |-- create_ext_sd.sh
|   `-- variscite_scripts
|       |-- echos.sh
|       `-- mx6ul_mx7_install_yocto.sh
`-- test.html
```
