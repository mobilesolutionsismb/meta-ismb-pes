SUMMARY = "LibNFC library"
DESCRIPTION = "libnfc is the first libre low level NFC SDK \
 and Programmers API released under the \
 GNU Lesser General Public License. "
HOMEPAGE = "http://nfc-tools.org/index.php/Main_Page"
SECTION = "libs"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING;md5=b52f2d57d10c4f7ee67a7eb9615d5d24"

SRC_URI = "https://github.com/nfc-tools/libnfc/releases/download/libnfc-${PV}/libnfc-${PV}.tar.bz2"
SRC_URI[md5sum] = "a3bea901778ac324e802b8ffb86820ff"
SRC_URI[sha256sum] = "945e74d8e27683f9b8a6f6e529557b305d120df347a960a6a7ead6cb388f4072"

#S = "${WORKDIR}/libnfc-${PV}"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append = " \
	file://libnfc.conf \
"

inherit autotools pkgconfig

DEPENDS = "libusb-compat"

do_install_append() {
	
	install -d ${D}${sysconfdir}/nfc/
	install -m 0755 ${WORKDIR}/libnfc.conf ${D}${sysconfdir}/nfc/libnfc.conf
}

FILES_${PN} += " \
	${sysconfdir}/nfc/libnfc.conf \
"