LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append = " \
	file://TagIcon.png \
	file://weston.ini \
	"
	
do_install_append() {
	
    install -d ${D}/usr/share/weston/
    install -m 0755 ${WORKDIR}/TagIcon.png ${D}/usr/share/weston/
    install -d ${D}/etc/xdg/weston/
    install -m 0755 ${WORKDIR}/weston.ini ${D}/etc/xdg/weston/
    
}

FILES_${PN} += " \
	/usr/share/weston/TagIcon.png \
	/etc/xdg/weston/weston.ini \
"