DESCRIPTION = "Pessione Spirit Minimal Core Image"
LICENSE = "MIT"

inherit core-image distro_features_check

# When not in debug, we can remove 'tools-debug'
IMAGE_FEATURES += " \
	debug-tweaks \
	hwcodecs \
	tools-testapps \
	package-management tools-debug ssh-server-dropbear \
"

CORE_IMAGE_EXTRA_INSTALL += " \
	packagegroup-fsl-tools-testapps \
	packagegroup-core-full-cmdline \
	packagegroup-core-buildessential \
	git \
    openssh-sftp-server \
    resolvconf \
    openssl \
    libnfc libfreefare udev-rules-pes \
    dtc \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'weston-init', '', d)} \
"