DESCRIPTION = "Freescale Image - Adds Qt5"
LICENSE = "MIT"

require fsl-core-pes.bb

inherit distro_features_check populate_sdk_qt5

CONFLICT_DISTRO_FEATURES = "directfb"

QT5_IMAGE_INSTALL_APPS = ""

QT5_FONTS = " \
    ttf-dejavu-mathtexgyre \
    ttf-dejavu-sans \
    ttf-dejavu-sans-condensed \
    ttf-dejavu-sans-mono \
    ttf-dejavu-serif \
    ttf-dejavu-serif-condensed \
    "
    
QT5_PACKAGES = " \
	qtbase qtbase-plugins \
	qtmultimedia \
	qtserialport \
"
QT5_WAYLAND = " \
	${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'qtwayland qtwayland-plugins', \
       bb.utils.contains('DISTRO_FEATURES',     'x11', '${QT5_IMAGE_INSTALL_APPS}', \
                                                       '', d), d)} \
"

IMAGE_INSTALL += " \
	${QT5_PACKAGES} \
	${QT5_FONTS} \
	${QT5_WAYLAND} \
	taskbar \
"

IMAGE_FSTYPES_remove = "ubi"